package garstka.jakub.zoovugameback.model.dao.impl

import garstka.jakub.zoovugameback.MongoSpringBootTest
import garstka.jakub.zoovugameback.logic.domains.GameDomain
import garstka.jakub.zoovugameback.model.dao.GameDao
import garstka.jakub.zoovugameback.utils.IdGenerator.newId
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.Instant
import java.time.temporal.ChronoUnit

private val basePlayedAt = Instant.now()

private val firstGameDomain = GameDomain(
        id = newId(),
        username = "firstUsername",
        gameResult = 1,
        penaltiesNumber = 1,
        playedAt = basePlayedAt.truncatedTo(ChronoUnit.MILLIS),
)

private val secondGameDomain = GameDomain(
        id = newId(),
        username = "secondUsername",
        gameResult = 2,
        penaltiesNumber = 2,
        playedAt = basePlayedAt.plus(2, ChronoUnit.HOURS).truncatedTo(ChronoUnit.MILLIS),
)

private val thirdGameDomain = GameDomain(
        id = newId(),
        username = "thirdUsername",
        gameResult = 3,
        penaltiesNumber = 2,
        playedAt = basePlayedAt.plus(3, ChronoUnit.HOURS).truncatedTo(ChronoUnit.MILLIS),
)

private val firstDateBeforePlayedAt = basePlayedAt.minus(1, ChronoUnit.DAYS)

private val firstDateBetweenPlayedAt = basePlayedAt.plus(1, ChronoUnit.HOURS)

private val firstDateAfterPlayedAt = basePlayedAt.plus(1, ChronoUnit.DAYS)
private val secondDateAfterPlayedAt = basePlayedAt.plus(2, ChronoUnit.DAYS)

//Blackbox tests focused mainly on GameDaoImpl and its integration with MongoDB
@MongoSpringBootTest
internal class GameDaoImplTest {

    @Autowired
    private lateinit var gameDao: GameDao

    @AfterEach
    fun clearCollection() {
        gameDao.removeAll()
    }

    @Test
    fun `should game be present in collection after saving it`() {
        //given
        //firstGameDomain -> is given

        //when
        val savedGameDomain = gameDao.save(firstGameDomain)
        val fetchedGameDomain = gameDao.getById(firstGameDomain.id)

        //then
        assertEquals(firstGameDomain, savedGameDomain)
        assertEquals(firstGameDomain, fetchedGameDomain)
    }

    @Test
    fun `should collection not have any previously inserted data`() {
        //given
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)

        //when
        gameDao.removeAll()
        val fetchedFirstGameDomain = gameDao.getById(firstGameDomain.id)
        val fetchedSecondGameDomain = gameDao.getById(secondGameDomain.id)

        //then
        assertNull(fetchedFirstGameDomain)
        assertNull(fetchedSecondGameDomain)
    }

    @Test
    fun `should data be in right order when fetching with pagination - ascending by result`() {
        //given
        val listInOrder = listOf(firstGameDomain, secondGameDomain).sortedBy { it.gameResult }
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)

        //when
        val fetchedListInOrder = gameDao.getWithPagination(0, 100, firstDateBeforePlayedAt, firstDateAfterPlayedAt)

        //then
        assertEquals(listInOrder, fetchedListInOrder)
    }

    @Test
    fun `should return no data when the skip is out of the actual elements amount`() {
        //given
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)

        //when
        val fetchedListInOrder = gameDao.getWithPagination(100, 100, firstDateBeforePlayedAt, firstDateAfterPlayedAt)

        //then
        assertEquals(emptyList<GameDomain>(), fetchedListInOrder)
    }

    @Test
    fun `should return data only in given range`() {
        //given
        val listInOrder = listOf(secondGameDomain, thirdGameDomain).sortedBy { it.gameResult }
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)
        gameDao.save(thirdGameDomain)

        //when
        val fetchedListInOrder = gameDao.getWithPagination(0, 100, firstDateBetweenPlayedAt, firstDateAfterPlayedAt)

        //then
        assertEquals(listInOrder, fetchedListInOrder)
    }

    @Test
    fun `should return data divided into two pages`() {
        //given
        val firstListInOrder = listOf(firstGameDomain, secondGameDomain).sortedBy { it.gameResult }
        val secondListInOrder = listOf(thirdGameDomain).sortedBy { it.gameResult }
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)
        gameDao.save(thirdGameDomain)

        //when
        val firstFetchedListInOrder = gameDao.getWithPagination(0, 2, firstDateBeforePlayedAt, firstDateAfterPlayedAt)
        val secondFetchedListInOrder = gameDao.getWithPagination(2, 2, firstDateBeforePlayedAt, firstDateAfterPlayedAt)

        //then
        assertEquals(firstListInOrder, firstFetchedListInOrder)
        assertEquals(secondListInOrder, secondFetchedListInOrder)
    }

    @Test
    fun `should return data divided into two pages but last page as empty due to lack of data in given date range`() {
        //given
        val firstListInOrder = listOf(firstGameDomain).sortedBy { it.gameResult }
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)
        gameDao.save(thirdGameDomain)

        //when
        val firstFetchedListInOrder = gameDao.getWithPagination(0, 2, firstDateBeforePlayedAt, firstDateBetweenPlayedAt)
        val secondFetchedListInOrder = gameDao.getWithPagination(2, 2, firstDateBeforePlayedAt, firstDateBetweenPlayedAt)

        //then
        assertEquals(firstListInOrder, firstFetchedListInOrder)
        assertEquals(emptyList<GameDomain>(), secondFetchedListInOrder)
    }

    @Test
    fun `should no elements be counted when given period is out of the playedAt fields range`() {
        //given
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)

        //when
        val fetchedCount = gameDao.count(firstDateAfterPlayedAt, secondDateAfterPlayedAt)

        //then
        assertEquals(0, fetchedCount)
    }

    @Test
    fun `should only one element be counted when given period is between the playedAt fields range`() {
        //given
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)

        //when
        val fetchedCount = gameDao.count(firstDateBeforePlayedAt, firstDateBetweenPlayedAt)

        //then
        assertEquals(1, fetchedCount)
    }

    @Test
    fun `should only one element be counted when given period is in the range of the playedAt fields`() {
        //given
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)

        //when
        val fetchedCount = gameDao.count(firstDateBeforePlayedAt, firstDateAfterPlayedAt)

        //then
        assertEquals(2, fetchedCount)
    }
}