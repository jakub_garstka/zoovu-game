package garstka.jakub.zoovugameback.view.controllers

import garstka.jakub.zoovugameback.MongoSpringBootTest
import garstka.jakub.zoovugameback.logic.domains.GameDomain
import garstka.jakub.zoovugameback.model.dao.GameDao
import garstka.jakub.zoovugameback.utils.IdGenerator
import io.restassured.RestAssured.given
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.Instant
import java.time.temporal.ChronoUnit

private val basePlayedAt = Instant.now()

private val firstGameDomain = GameDomain(
        id = IdGenerator.newId(),
        username = "firstUsername",
        gameResult = 1,
        penaltiesNumber = 1,
        playedAt = basePlayedAt.truncatedTo(ChronoUnit.MILLIS),
)

private val secondGameDomain = GameDomain(
        id = IdGenerator.newId(),
        username = "secondUsername",
        gameResult = 2,
        penaltiesNumber = 2,
        playedAt = basePlayedAt.plus(2, ChronoUnit.HOURS).truncatedTo(ChronoUnit.MILLIS),
)

private val thirdGameDomain = GameDomain(
        id = IdGenerator.newId(),
        username = "thirdUsername",
        gameResult = 3,
        penaltiesNumber = 2,
        playedAt = basePlayedAt.plus(3, ChronoUnit.HOURS).truncatedTo(ChronoUnit.MILLIS),
)

private val firstDateBeforePlayedAt = basePlayedAt.minus(1, ChronoUnit.DAYS)

private val firstDateBetweenPlayedAt = basePlayedAt.plus(1, ChronoUnit.HOURS)

private val firstDateAfterPlayedAt = basePlayedAt.plus(1, ChronoUnit.DAYS)


@MongoSpringBootTest
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
internal class GameControllerTest {

    @Autowired
    private lateinit var gameDao: GameDao

    @AfterEach
    fun clearCollection() {
        gameDao.removeAll()
    }

    @Test
    fun `should game be saved and 200 status code returned as well as domain object`() {

        val savedGame = given()
                .contentType("application/json")
                .body(firstGameDomain)
                .`when`()
                .post("/games")
                .then()
                .statusCode(200)
                .extract().`as`(GameDomain::class.java)

        val fetchedGame = gameDao.getById(firstGameDomain.id)

        assertEquals(firstGameDomain, savedGame)
        assertEquals(fetchedGame, savedGame)
    }

    @Test
    fun `should fetch elements only in a given range and count them`() {
        val listInOrder = listOf(secondGameDomain, thirdGameDomain).sortedBy { it.gameResult }
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)
        gameDao.save(thirdGameDomain)

        val response = given()
                .param("from", firstDateBetweenPlayedAt.toString())
                .param("to", firstDateAfterPlayedAt.toString())
                .`when`()
                .get("/games")
                .then()
                .statusCode(200)
                .extract()


        val list = response.body().jsonPath().getList("list", GameDomain::class.java)
        val count = response.body().jsonPath().get<Int>("count")

        assertEquals(listInOrder, list)
        assertEquals(2, count)
    }

    @Test
    fun `should fetch elements only in a given range, also page, and count them - first page`() {
        val listInOrder = listOf(firstGameDomain, secondGameDomain).sortedBy { it.gameResult }
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)
        gameDao.save(thirdGameDomain)

        val response = given()
                .param("skip", 0)
                .param("limit", 2)
                .param("from", firstDateBeforePlayedAt.toString())
                .param("to", firstDateAfterPlayedAt.toString())
                .`when`()
                .get("/games")
                .then()
                .statusCode(200)
                .extract()


        val list = response.body().jsonPath().getList("list", GameDomain::class.java)
        val count = response.body().jsonPath().get<Int>("count")

        assertEquals(listInOrder, list)
        assertEquals(3, count)
    }

    @Test
    fun `should fetch elements only in a given range, also page, and count them - second page`() {
        val listInOrder = listOf(thirdGameDomain).sortedBy { it.gameResult }
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)
        gameDao.save(thirdGameDomain)

        val response = given()
                .param("skip", 2)
                .param("limit", 2)
                .param("from", firstDateBeforePlayedAt.toString())
                .param("to", firstDateAfterPlayedAt.toString())
                .`when`()
                .get("/games")
                .then()
                .statusCode(200)
                .extract()


        val list = response.body().jsonPath().getList("list", GameDomain::class.java)
        val count = response.body().jsonPath().get<Int>("count")

        assertEquals(listInOrder, list)
        assertEquals(3, count)
    }

    @Test
    fun `should fetch no elements in a given range due to lack of elements on the page, but every game in the range should be counted`() {
        gameDao.save(firstGameDomain)
        gameDao.save(secondGameDomain)
        gameDao.save(thirdGameDomain)

        val response = given()
                .param("skip", 100)
                .param("limit", 100)
                .param("from", firstDateBeforePlayedAt.toString())
                .param("to", firstDateAfterPlayedAt.toString())
                .`when`()
                .get("/games")
                .then()
                .statusCode(200)
                .extract()


        val list = response.body().jsonPath().getList("list", GameDomain::class.java)
        val count = response.body().jsonPath().get<Int>("count")

        assertEquals(emptyList<GameDomain>(), list)
        assertEquals(3, count)
    }
}