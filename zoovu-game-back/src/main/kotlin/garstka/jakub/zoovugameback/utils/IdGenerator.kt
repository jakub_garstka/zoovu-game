package garstka.jakub.zoovugameback.utils

import org.bson.types.ObjectId

object IdGenerator {
    fun newId(): String = ObjectId().toHexString()
}