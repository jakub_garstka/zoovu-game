package garstka.jakub.zoovugameback.utils

import garstka.jakub.zoovugameback.logic.domains.GameDomain
import garstka.jakub.zoovugameback.model.entities.GameEntity
import org.litote.kmongo.id.WrappedObjectId

fun GameDomain.toEntity(): GameEntity = GameEntity(
        _id = WrappedObjectId(id),
        username = username,
        gameResult = gameResult,
        penaltiesNumber = penaltiesNumber,
        playedAt = playedAt,
)

fun GameEntity.toDomain(): GameDomain = GameDomain(
        id = _id.toString(),
        username = username,
        gameResult = gameResult,
        penaltiesNumber = penaltiesNumber,
        playedAt = playedAt,
)