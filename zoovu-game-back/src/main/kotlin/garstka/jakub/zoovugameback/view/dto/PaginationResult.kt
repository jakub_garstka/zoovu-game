package garstka.jakub.zoovugameback.view.dto

data class PaginationResult<T>(val list: List<T>, val count: Long)