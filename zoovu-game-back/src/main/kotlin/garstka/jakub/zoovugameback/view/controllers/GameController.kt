package garstka.jakub.zoovugameback.view.controllers

import garstka.jakub.zoovugameback.logic.domains.GameDomain
import garstka.jakub.zoovugameback.logic.services.GameService
import garstka.jakub.zoovugameback.utils.DEFAULT_LIMIT
import garstka.jakub.zoovugameback.utils.DEFAULT_SKIP
import garstka.jakub.zoovugameback.view.dto.PaginationResult
import org.springframework.web.bind.annotation.*
import java.time.Instant

@RestController
//Only for development purpose
@CrossOrigin("http://localhost:3000")
@RequestMapping("games", produces = ["application/json"])
class GameController(private val gameService: GameService) {

    @PostMapping(consumes = ["application/json"])
    fun save(@RequestBody domain: GameDomain) = gameService.save(domain)

    @GetMapping
    fun getWithDefaultPagination(
            @RequestParam(required = false) skip: Int?,
            @RequestParam(required = false) limit: Int?,
            @RequestParam(required = false) from: Instant?,
            @RequestParam(required = false) to: Instant?,
    ) = PaginationResult(
            list = gameService.getWithPagination(skip ?: DEFAULT_SKIP, limit ?: DEFAULT_LIMIT, from, to),
            count = gameService.count(from, to)
    )


}