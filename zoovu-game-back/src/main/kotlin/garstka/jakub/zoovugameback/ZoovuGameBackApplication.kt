package garstka.jakub.zoovugameback

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ZoovuGameBackApplication

fun main(args: Array<String>) {
    runApplication<ZoovuGameBackApplication>(*args)
}
