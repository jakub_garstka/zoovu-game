package garstka.jakub.zoovugameback.config

import com.mongodb.client.MongoDatabase
import org.litote.kmongo.KMongo
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MongoDatabaseConfig(
        private val mongoProperties: MongoProperties
) {

    private val mongoClient = KMongo.createClient(mongoProperties.url)

    @Bean
    fun mongoDatabase(): MongoDatabase {
        return mongoClient.getDatabase(mongoProperties.database)
    }

}