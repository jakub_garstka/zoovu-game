package garstka.jakub.zoovugameback.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "mongo")
class MongoProperties {
    lateinit var url: String
    lateinit var database: String
}