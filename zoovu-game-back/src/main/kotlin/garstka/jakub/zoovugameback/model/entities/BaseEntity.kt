package garstka.jakub.zoovugameback.model.entities

import org.litote.kmongo.Id

interface BaseEntity<T> {
    val _id: Id<T>
}