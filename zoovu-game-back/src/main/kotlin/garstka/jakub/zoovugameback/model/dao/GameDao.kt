package garstka.jakub.zoovugameback.model.dao

import garstka.jakub.zoovugameback.logic.domains.GameDomain
import java.time.Instant

interface GameDao : BaseDao<GameDomain> {
    fun getWithPagination(skip: Int, limit: Int, from: Instant?, to: Instant?): List<GameDomain>
    fun count(from: Instant?, to: Instant?): Long
}