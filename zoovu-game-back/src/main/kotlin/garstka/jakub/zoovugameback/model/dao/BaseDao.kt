package garstka.jakub.zoovugameback.model.dao


interface BaseDao<T> {
    fun save(domain: T): T
    fun getById(id: String): T?
    fun removeAll()

}