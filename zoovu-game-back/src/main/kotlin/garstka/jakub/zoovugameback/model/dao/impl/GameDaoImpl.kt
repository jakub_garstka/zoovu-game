package garstka.jakub.zoovugameback.model.dao.impl

import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.FindOneAndReplaceOptions
import com.mongodb.client.model.ReturnDocument
import garstka.jakub.zoovugameback.logic.domains.GameDomain
import garstka.jakub.zoovugameback.model.dao.GameDao
import garstka.jakub.zoovugameback.model.entities.GameEntity
import garstka.jakub.zoovugameback.utils.toDomain
import garstka.jakub.zoovugameback.utils.toEntity
import org.litote.kmongo.*
import org.litote.kmongo.id.WrappedObjectId
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class GameDaoImpl(
        mongoDatabase: MongoDatabase
) : GameDao {

    val collection = mongoDatabase.getCollection<GameEntity>()

    override fun save(domain: GameDomain): GameDomain =
            collection.findOneAndReplace(
                    GameEntity::_id eq WrappedObjectId(domain.id),
                    domain.toEntity(),
                    FindOneAndReplaceOptions().upsert(true).returnDocument(ReturnDocument.AFTER)
            )!!.toDomain()

    override fun getById(id: String): GameDomain? = collection.findOneById(WrappedObjectId<GameEntity>(id))?.toDomain()

    override fun removeAll() = collection.drop()

    override fun getWithPagination(skip: Int, limit: Int, from: Instant?, to: Instant?): List<GameDomain> {
        val filterConditions = listOfNotNull(
                from?.let { GameEntity::playedAt gte from },
                to?.let { GameEntity::playedAt lt to }
        )

        val filter = and(filterConditions)
        val sort = ascending(GameEntity::gameResult)

        return collection.find(filter).skip(skip).limit(limit).sort(sort).map { it.toDomain() }.toList()
    }

    override fun count(from: Instant?, to: Instant?): Long {
        val filterConditions = listOfNotNull(
                from?.let { GameEntity::playedAt gte from },
                to?.let { GameEntity::playedAt lt to }
        )

        val filter = and(filterConditions)

        return collection.countDocuments(filter)
    }
}