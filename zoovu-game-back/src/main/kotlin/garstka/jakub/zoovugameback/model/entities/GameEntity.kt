package garstka.jakub.zoovugameback.model.entities

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import org.litote.kmongo.Id
import java.time.Instant

@Serializable
data class GameEntity(
        @Contextual
        override val _id: Id<GameEntity>,
        val username: String,
        //in seconds
        val gameResult: Int,
        val penaltiesNumber: Int,
        @Contextual
        val playedAt: Instant,
) : BaseEntity<GameEntity>
