package garstka.jakub.zoovugameback.logic.domains

interface BaseDomain {
    val id: String
}