package garstka.jakub.zoovugameback.logic.services

import garstka.jakub.zoovugameback.logic.domains.GameDomain
import garstka.jakub.zoovugameback.model.dao.GameDao
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class GameService(
        private val gameDao: GameDao
) {

    fun save(domain: GameDomain) = gameDao.save(domain)

    fun getWithPagination(skip: Int, limit: Int, from: Instant?, to: Instant?) =
            gameDao.getWithPagination(skip, limit, from, to)

    fun count(from: Instant?, to: Instant?) =
            gameDao.count(from, to)
}