package garstka.jakub.zoovugameback.logic.domains

import garstka.jakub.zoovugameback.utils.IdGenerator.newId
import java.time.Instant

data class GameDomain(
        override val id: String = newId(),
        val username: String,
        //in seconds
        val gameResult: Int,
        val penaltiesNumber: Int,
        val playedAt: Instant,
) : BaseDomain