# Zoovu Game - Backend Application

## Requirements

For building and running the application you need:

- [JDK 1.11](https://www.oracle.com/java/technologies/javase-downloads.html#JDK11)
- [Gradle 6.7.1](https://gradle.org/)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `zoovu-game-back.src.main.kotlin.garstkajakub.zoovugameback.ZoovuGameBackApplication.kt` class from your IDE.

Alternatively you can use the [Spring Boot Gradle plugin](https://docs.spring.io/spring-boot/docs/current/gradle-plugin/reference/htmlsingle/#introduction) like so:

```bash
./gradlew bootRun 
```

The application should be accessible on [http://localhost:8080/](http://localhost:8080/)


## Dependencies

### [KMongo](https://litote.org/kmongo/)
KMongo is a Kotlin toolkit for MongoDB. It prefers Objects rather than Maps. Moreover, It natively supports Kotlin serialization which is kotlin-first, compile-time, type-safe, reflection-free and completely multi-platform ready serialization mechanism to convert kotlin objects into data formats like JSON or Protobuf and vice-versa.
 
### [Swagger](https://swagger.io/)
Swagger is a set of rules of describing REST APIs. It ensures the format readable by machines as well as humans. It can be used to share documentation between a team members. For example, communication between back-end and front-end developers can be highly improved by this tool due to transparency of the actual REST API structure.

### [Testcontainers](https://www.testcontainers.org/)
Library that ensures DB docker container for integration tests purpose. It supports JUnit tests and has plugin as `testcontainers:mongodb` to create MongoDB instance. As it was mentioned, the library was created especially for integration tests so in the project it is highly used in black-box tests of the connection with database.

### [Rest-assured](https://rest-assured.io/)
Library dedicated to tests for RESTful APIs. It contains highly descriptive syntax that enables developer to follow through test structure by the key words like `given, when, then`.