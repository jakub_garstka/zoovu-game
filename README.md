# Zoovu Game

Detailed README.md files are placed in the folders `zoovu-game-back` and `zoovu-game-front`. There you can find more information of technical aspects of the application.

## Deployment

The application is currenty deployed at [Heroku](https://dashboard.heroku.com/). Feel free to experiment!

[https://zoovu-game-jg.herokuapp.com/game](https://zoovu-game-jg.herokuapp.com/game)
