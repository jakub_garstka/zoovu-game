# Zoovu Game - Frontend Application

Zoovu Game is simple DnD application enabling users to solve simple puzzles as well as saving their result in a global ranking.

## To run locally

Use the package manager [yarn](https://yarnpkg.com/) or [npm](https://www.npmjs.com/) to install all needed dependencies.

### yarn
```bash
yarn install
yarn start
```

### npm
```bash
npm i
npm start
```

The application should be accessible on [http://localhost:3000/](http://localhost:3000/)

## Dependiencies

### [XState](https://github.com/davidkpiano/xstate)
To provide future developers highly extensible base of the code XState library was used to implement simple state machine. This approach ensures that the code is highly descriptive by finite states and some additional actions that run on entering / exiting every state.

### [react-beautiful-dnd](https://github.com/atlassian/react-beautiful-dnd) 
react-beautiful-dnd is a library that provides beautiful drag and drop experience. Basically, It is usually used with companion of lists but by few nonstandard mechanisms it gives different feeling, it feels great with game rules.

### [Styled components](https://styled-components.com/)
For purpose of the project styled-components were picked as a CSS in JS library to provide styling. Thanks to such approach, we don't have to worry about global class names (the source of bugs). As the name suggests, this technic encourage to use small reusable piece of styles / components which is highly connected of React way of defining UI by small pieces, also called components. What is more, styled components gives us opportunity to ensure dynamic styling by injecting props.

### [axios](https://github.com/axios/axios)
Promise based HTTP client for the browser and node.js.

### [Reach Router](https://reach.tech/router/)
Reach Router is router library supporting only the simplest route patters by design.