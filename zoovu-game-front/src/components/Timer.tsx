import React, { useEffect, useState } from 'react';
import useEffectsCleanUp from '../hooks/custom/useEffectsCleanUp';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import styled from 'styled-components';

const TimerContainer = styled.div`
    position: relative;
    display: flex;
    flex-flow: row wrap;
    align-items: center;
    font-size: ${(props) => props.theme.typography.size.large};
    margin: 1rem 0;
`;

const TimerText = styled.span`
    && {
        color: ${(props) => props.theme.colors.violet};
        font-size: inherit !important;
    }
`;

const CustomTimerIcon = styled(AccessTimeIcon)`
    color: ${(props) => props.theme.colors.green};
    font-size: inherit !important;
`;

interface AddedTimeEffectContainerProps {
    duration: number;
}

const AddedTimeEffectContainer = styled.div<AddedTimeEffectContainerProps>`
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;

    span {
        position: absolute;
        top: 0;
        right: 0;
        color: ${(props) => props.theme.colors.violet};
        opacity: 0;
        animation-name: ripple;
        animation-duration: ${(props) => props.duration}ms;
    }

    @keyframes ripple {
        100% {
            opacity: 0;
            top: 0rem;
        }
        50% {
            opacity: 0.5;
        }
        0% {
            opacity: 0;
            top: 1rem;
        }
    }
`;

//IN ms
const EFFECT_DURATION = 750;

interface TimerProps {
    seconds: number;
    penalties: number;
}

const Timer: React.FC<TimerProps> = ({ seconds, penalties }: TimerProps) => {
    const [innerPenalties, setInnerPenalties] = useState(penalties);

    useEffectsCleanUp(innerPenalties, EFFECT_DURATION, () => {
        setInnerPenalties(0);
    });

    useEffect(() => {
        if (penalties > 0) {
            setInnerPenalties((prevState) => prevState + 1);
        }
    }, [penalties]);

    return (
        <TimerContainer>
            <AddedTimeEffectContainer duration={EFFECT_DURATION}>
                {Array(innerPenalties)
                    .fill(0)
                    .map((_, index) => (
                        <span key={index}>+ 10 seconds</span>
                    ))}
            </AddedTimeEffectContainer>
            <CustomTimerIcon />
            <TimerText>
                Your score:
                {` ${seconds} seconds`}
            </TimerText>
        </TimerContainer>
    );
};

export default Timer;
