import { Draggable } from 'react-beautiful-dnd';
import SquareImageCard from '../cards/SquareImageCard';
import React from 'react';

export interface DraggableLetterProps {
    id: string;
    url: string;
    index: number;
    isDragDisabled: boolean;
}

const DraggableLetter: React.FC<DraggableLetterProps> = ({ id, url, index, isDragDisabled }: DraggableLetterProps) => {
    return (
        <Draggable draggableId={id} index={index} isDragDisabled={isDragDisabled}>
            {(provided) => (
                <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                    <SquareImageCard url={url} />
                </div>
            )}
        </Draggable>
    );
};

export default DraggableLetter;
