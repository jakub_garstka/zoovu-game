import React from 'react';
import { Droppable } from 'react-beautiful-dnd';
import styled from 'styled-components';

interface LetterDroppableProps {
    id: string;
    isDropDisabled: boolean;
    children: React.ReactNode;
}

const PlaceholderContainer = styled.span`
    display: none;
`;

const DroppableLetter: React.FC<LetterDroppableProps> = ({ id, children, isDropDisabled }: LetterDroppableProps) => {
    return (
        <Droppable droppableId={id} isDropDisabled={isDropDisabled} direction="vertical">
            {(provided) => (
                <div id={id} ref={provided.innerRef} {...provided.droppableProps}>
                    {children}
                    <PlaceholderContainer>{provided.placeholder}</PlaceholderContainer>
                </div>
            )}
        </Droppable>
    );
};

export default DroppableLetter;
