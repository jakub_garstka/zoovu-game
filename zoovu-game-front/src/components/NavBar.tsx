import React from 'react';
import styled from 'styled-components';
import { Link, RouteComponentProps } from '@reach/router';

const LinksList = styled.ul`
    display: flex;
    flex-flow: row wrap;
    gap: 1rem;
    padding: unset;
    padding: 0 1rem;
`;

const LinkItem = styled.li`
    font-weight: ${(props) => props.theme.typography.weight.bold};
    list-style: none;
    text-align: center;
    
    
    a {
        background: linear-gradient(-45deg, ${(props) => `${props.theme.colors.green}, ${props.theme.colors.violet}`});
        background-size: 400% 400%;
        transition: all ease-in-out .3s;
        background-size: 1000% 1000%;
        text-decoration: none;
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }
    
    @keyframes gradient {
        0% {
            background-position: 0% 50%;
        }
        50% {
            background-position: 100% 50%;
        }
        100% {
            background-position: 0% 50%;
        }
`;

interface NavBarProps extends RouteComponentProps {
    children: React.ReactNode;
}

const prepareActiveLink = (isCurrent: boolean) => {
    return {
        style: {
            animation: isCurrent ? 'gradient 3s ease infinite' : 'none',
        },
    };
};

const NavBar: React.FC<NavBarProps> = ({ children }: NavBarProps) => {
    return (
        <>
            <header>
                <nav>
                    <LinksList>
                        <LinkItem>
                            <Link to="/" getProps={({ isCurrent }) => prepareActiveLink(isCurrent)}>
                                Leader board
                            </Link>
                        </LinkItem>
                        <LinkItem>
                            <Link to="/game" getProps={({ isCurrent }) => prepareActiveLink(isCurrent)}>
                                Game
                            </Link>
                        </LinkItem>
                    </LinksList>
                </nav>
            </header>
            <main>{children}</main>
        </>
    );
};

export default NavBar;
