import React from 'react';
import { Chip, Dialog } from '@material-ui/core';
import { DateRange, DateRangePicker } from 'materialui-daterange-picker/dist';
import { formatDate } from '../utils/utils';
import { ALL, DATE } from '../common/consts';
import styled from 'styled-components';

const RangeText = styled.span`
    font-weight: ${(props) => props.theme.typography.weight.bold};
`;

const displayDataRange = (dataRange?: DateRange): string => {
    if (!dataRange) return `${ALL}`;

    const { startDate, endDate } = dataRange;

    const range = ([startDate, endDate].filter((value) => value !== null && value !== undefined) as Date[])
        .map((value) => formatDate(value, false))
        .join(' - ');

    return `${range}`;
};

interface DataRangePickerProps {
    dataRange?: DateRange;
    onChange: (dateRange?: DateRange) => void;
    minDate?: Date | string;
    maxDate?: Date | string;
}

const DataRangePicker: React.FC<DataRangePickerProps> = ({
    dataRange,
    onChange,
    minDate,
    maxDate,
}: DataRangePickerProps) => {
    const [open, setOpen] = React.useState(false);
    const toggle = () => setOpen(!open);

    return (
        <>
            <Dialog disableBackdropClick open={open} onEscapeKeyDown={toggle} maxWidth={false} onBackdropClick={toggle}>
                <DateRangePicker
                    minDate={minDate}
                    maxDate={maxDate}
                    toggle={toggle}
                    initialDateRange={dataRange}
                    open={true}
                    onChange={onChange}
                />
            </Dialog>
            <Chip
                color={dataRange ? 'secondary' : 'default'}
                onClick={toggle}
                onDelete={dataRange && (() => onChange())}
                label={
                    <span>
                        {DATE}: <RangeText>{displayDataRange(dataRange)}</RangeText>
                    </span>
                }
            />
        </>
    );
};

export default DataRangePicker;
