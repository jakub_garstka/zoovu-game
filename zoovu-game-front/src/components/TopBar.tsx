import Timer from './Timer';
import React from 'react';
import styled from 'styled-components';

const TopBarContainer = styled.div`
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
`;

interface TopBarColumnProps {
    alignItems: 'flex-end' | 'flex-start' | 'center';
}

const TopBarColumn = styled.div<TopBarColumnProps>`
    display: flex;
    flex-flow: column;
    align-items: ${(props) => props.alignItems};
    margin: 1rem;
`;

const TopBarGreetings = styled.span`
    font-size: ${(props) => props.theme.typography.size.large};
    font-weight: ${(props) => props.theme.typography.weight.bold};
    margin: 1rem 0;
`;

const TopBarSecondaryText = styled.span`
    color: ${(props) => props.theme.colors.gray};
`;

interface TopBarProps {
    username: string;
    seconds: number;
    penalties: number;
}

const TopBar: React.FC<TopBarProps> = ({ username, seconds, penalties }: TopBarProps) => {
    return (
        <TopBarContainer>
            <TopBarColumn alignItems={'flex-start'}>
                <TopBarGreetings>Good luck, {username}!</TopBarGreetings>
                <TopBarSecondaryText>Pick up the right cards</TopBarSecondaryText>
            </TopBarColumn>
            <TopBarColumn alignItems={'flex-end'}>
                <Timer seconds={seconds} penalties={penalties} />
                <TopBarSecondaryText>The faster the better!</TopBarSecondaryText>
            </TopBarColumn>
        </TopBarContainer>
    );
};

export default TopBar;
