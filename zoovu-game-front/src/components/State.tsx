import React from 'react';
import styled from 'styled-components';
import { Button } from '@material-ui/core';
import { REFETCH_DATA } from '../common/consts';

const Container = styled.div`
    padding: 2rem;
    display: grid;
    place-items: center;
`;

const Message = styled.span`
    padding: 1rem;
    font-size: ${(props) => props.theme.typography.size.normal};
    font-weight: ${(props) => props.theme.typography.weight.bold};
`;

const Image = styled.img`
    width: 25%;
    max-width: 400px;
`;

interface StateProps {
    message: string;
    url: string;
    refetchData: () => void;
    buttonText?: string;
}

const State: React.FC<StateProps> = ({ message, url, refetchData, buttonText = REFETCH_DATA }: StateProps) => {
    return (
        <Container>
            <Image src={url} />
            <Message>{message}</Message>
            <Button color="primary" variant="contained" onClick={refetchData}>
                {buttonText}
            </Button>
        </Container>
    );
};

export default State;
