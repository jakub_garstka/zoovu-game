import styled from 'styled-components';
import SquareCard from './SquareCard';

const resolveBorderColor = (type: 'primary' | 'secondary', sourceColor: string, targetColor: string): string => {
    const color = type === 'primary' ? sourceColor : targetColor;
    return color.replace('#', '%23');
};

interface SquareBorderedCardProps {
    type: 'primary' | 'secondary';
}

const SquareBorderedCard = styled(SquareCard)<SquareBorderedCardProps>`
    && {
        overflow: unset;
        box-shadow: none;
        background-image: url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='20' ry='20' stroke='${(
            props,
        ) =>
            resolveBorderColor(
                props.type,
                props.theme.colors.gray,
                props.theme.colors.green,
            )}' stroke-width='3' stroke-dasharray='10' stroke-dashoffset='0' stroke-linecap='butt'/%3e%3c/svg%3e");
    }
`;

export default SquareBorderedCard;
