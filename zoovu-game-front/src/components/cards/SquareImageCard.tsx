import styled from 'styled-components';
import SquareCard from './SquareCard';

interface SquareImageCardProps {
    url: string;
}

const SquareImageCard = styled(SquareCard)<SquareImageCardProps>`
    && {
        background-color: ${(props) => props.theme.colors.white};
        background-image: url(${(props) => props.url});
        background-position: center;
        background-repeat: no-repeat;
    }
`;

export default SquareImageCard;
