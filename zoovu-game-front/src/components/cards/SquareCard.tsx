import { Card } from '@material-ui/core';
import styled from 'styled-components';

const SquareCard = styled(Card)`
    && {
        width: 10rem;
        height: 10rem;
    }
`;

export default SquareCard;
