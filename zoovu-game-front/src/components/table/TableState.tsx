import React from 'react';
import TableStateRows from './TableStateRows';
import ErrorState from '../../assets/error-state.svg';
import EmptyState from '../../assets/empty-state.svg';

interface TableStateProps {
    error: boolean;
    isEmpty: boolean;
    errorMessage: string;
    emptyMessage: string;
    rowsPerPage: number;
    colSpan: number;
    refetchData: () => void;
}

const TableState: React.FC<TableStateProps> = ({
    error,
    isEmpty,
    errorMessage,
    emptyMessage,
    colSpan,
    rowsPerPage,
    refetchData,
}: TableStateProps) => {
    if (error)
        return (
            <TableStateRows
                show={true}
                url={ErrorState}
                message={errorMessage}
                colSpan={colSpan}
                rowsPerPage={rowsPerPage}
                refetchData={refetchData}
            />
        );

    if (isEmpty)
        return (
            <TableStateRows
                show={true}
                url={EmptyState}
                message={emptyMessage}
                colSpan={colSpan}
                rowsPerPage={rowsPerPage}
                refetchData={refetchData}
            />
        );

    return <></>;
};

export default TableState;
