import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    flex-flow: column wrap;
    padding: 1rem;
`;

const TitleContainer = styled.div`
    display: flex;
    font-size: ${(props) => props.theme.typography.size.large};
`;

interface FiltersContainersProps {
    maxWidth: string;
}

const FiltersContainers = styled.div<FiltersContainersProps>`
    padding-top: 1rem;
    display: flex;
    flex-flow: row wrap;
    gap: 1rem;
    justify-content: flex-end;
    max-width: ${(props) => props.maxWidth};
`;

interface TableToolbarProps {
    title: string;
    children: React.ReactNode;
}

const TableToolbar: React.FC<TableToolbarProps> = ({ title, children }: TableToolbarProps) => {
    const [maxWidth, setMaxWidth] = useState('unset');
    const ref = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (ref.current) {
            const width = ref.current.getBoundingClientRect().width;
            setMaxWidth(`${width}px`);
        }
    }, []);

    return (
        <Container ref={ref}>
            <TitleContainer>{title}</TitleContainer>
            <FiltersContainers maxWidth={maxWidth}>{children}</FiltersContainers>
        </Container>
    );
};

export default TableToolbar;
