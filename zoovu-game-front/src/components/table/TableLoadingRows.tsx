import React from 'react';
import styled from 'styled-components';
import { TableCell, TableRow } from '@material-ui/core';

const TableLoadingCell = styled(TableCell)`
    position: relative;

    &:before {
        background: linear-gradient(-45deg, ${(props) => `${props.theme.colors.green}, ${props.theme.colors.gray}`});
        background-size: 400% 400%;
        animation: gradient 3s ease infinite;
        opacity: 0.1;
        border-radius: ${(props) => props.theme.borderRadius};
        content: '';
        position: absolute;
        top: 0.75rem;
        bottom: 0.75rem;
        right: 1rem;
        left: 1rem;
    }
    
    @keyframes gradient {
        0% {
            background-position: 0% 50%;
        }
        50% {
            background-position: 100% 50%;
        }
        100% {
            background-position: 0% 50%;
        }
`;

interface TableLoadingRowsProps {
    show: boolean;
    rowsPerPage: number;
    colSpan: number;
}

const TableLoadingRows: React.FC<TableLoadingRowsProps> = ({ show, rowsPerPage, colSpan }: TableLoadingRowsProps) => {
    return (
        <>
            {show &&
                Array(rowsPerPage)
                    .fill(0)
                    .map((_, index) => (
                        <TableRow key={index} style={{ height: 53 }}>
                            <TableLoadingCell colSpan={colSpan} />
                        </TableRow>
                    ))}
        </>
    );
};

export default TableLoadingRows;
