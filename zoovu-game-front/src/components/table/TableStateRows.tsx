import React from 'react';
import { TableCell, TableRow } from '@material-ui/core';
import State from '../State';

interface TableStateRowsProps {
    show: boolean;
    message: string;
    url: string;
    rowsPerPage: number;
    colSpan: number;
    refetchData: () => void;
}

const TableStateRows: React.FC<TableStateRowsProps> = ({
    show,
    message,
    url,
    rowsPerPage,
    colSpan,
    refetchData,
}: TableStateRowsProps) => {
    return (
        <>
            {show && (
                <TableRow style={{ height: 53 * rowsPerPage }}>
                    <TableCell colSpan={colSpan}>
                        <State url={url} message={message} refetchData={refetchData} />
                    </TableCell>
                </TableRow>
            )}
        </>
    );
};

export default TableStateRows;
