import React from 'react';
import { TableCell, TableRow } from '@material-ui/core';

interface TableEmptyRowsProps {
    show: boolean;
    emptyRowsNumber: number;
    colSpan: number;
}

const TableEmptyRows: React.FC<TableEmptyRowsProps> = ({ show, emptyRowsNumber, colSpan }: TableEmptyRowsProps) => {
    return (
        <>
            {show && emptyRowsNumber > 0 && (
                <TableRow style={{ height: 53 * emptyRowsNumber }}>
                    <TableCell colSpan={colSpan} />
                </TableRow>
            )}
        </>
    );
};

export default TableEmptyRows;
