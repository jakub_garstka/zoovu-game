import React from 'react';
import { Button, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import styled from 'styled-components';
import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';

const SuccessDialogContent = styled(DialogContent)`
    && {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 0.5rem;
    }
`;

const CustomCheckCircleOutline = styled(CheckCircleOutline)`
    && {
        color: ${(props) => props.theme.colors.green};
        font-size: 5rem;
    }
`;

const GreetingsContainer = styled.div`
    color: ${(props) => props.theme.colors.violet};
    font-size: ${(props) => props.theme.typography.size.large};
`;

const ResultContainer = styled(DialogContent)`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const SecondsContainer = styled.div`
    color: ${(props) => props.theme.colors.black};
    font-size: ${(props) => props.theme.typography.size.normal};
`;

const MistakesContainer = styled.div`
    color: ${(props) => props.theme.colors.gray};
    font-size: ${(props) => props.theme.typography.size.small};
`;

interface SuccessModalProps {
    timerSeconds: number;
    penaltiesNumber: number;
    username: string;
    isGameFinished: boolean;
    isGameSaved: boolean;
    restartGame: () => void;
    saveGame: () => void;
}

const SuccessModal: React.FC<SuccessModalProps> = ({
    timerSeconds,
    penaltiesNumber,
    username,
    isGameFinished,
    isGameSaved,
    restartGame,
    saveGame,
}: SuccessModalProps) => {
    return (
        <Dialog disableBackdropClick disableEscapeKeyDown maxWidth="sm" open={isGameFinished}>
            <SuccessDialogContent>
                <CustomCheckCircleOutline />
                <GreetingsContainer>{username}, congratulations!</GreetingsContainer>
                <ResultContainer>
                    <SecondsContainer>Your result: {timerSeconds} seconds</SecondsContainer>
                    <MistakesContainer>Mistakes number: {penaltiesNumber}</MistakesContainer>
                </ResultContainer>
            </SuccessDialogContent>
            <DialogActions>
                <Button color="secondary" onClick={saveGame} disabled={isGameSaved}>
                    Save result
                </Button>
                <Button color="primary" onClick={restartGame}>
                    Restart
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default SuccessModal;
