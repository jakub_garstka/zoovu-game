import 'styled-components';

declare module 'styled-components' {
    export interface DefaultTheme {
        borderRadius: string;
        boxShadow: string;
        colors: {
            white: string;
            black: string;
            gray: string;
            violet: string;
            green: string;
        };
        typography: {
            size: {
                small: string;
                normal: string;
                large: string;
            };
            weight: {
                light: string;
                bold: string;
            };
        };
    }
}
