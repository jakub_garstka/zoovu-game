import React from 'react';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { muiTheme, theme } from './styles/theme';
import GlobalStyle from './styles/global-styles';
import Board from './pages/Board';
import { Router } from '@reach/router';
import { UsernameProvider } from './providers/UsernameProvider';
import { ApiProvider } from './providers/ApiProvider';
import { SnackbarProvider } from 'notistack';
import NavBar from './components/NavBar';
import LeaderBoard from './pages/LeaderBoard';

const App: React.FC = () => {
    return (
        <StyledThemeProvider theme={theme}>
            <MuiThemeProvider theme={muiTheme}>
                <GlobalStyle />
                <SnackbarProvider maxSnack={3}>
                    <ApiProvider>
                        <UsernameProvider>
                            <Router>
                                <NavBar path="/">
                                    <LeaderBoard default />
                                    <Board path="/game" />
                                </NavBar>
                            </Router>
                        </UsernameProvider>
                    </ApiProvider>
                </SnackbarProvider>
            </MuiThemeProvider>
        </StyledThemeProvider>
    );
};

export default App;
