import React, { FormEvent, useState } from 'react';
import styled from 'styled-components';
import { Button, Card, TextField } from '@material-ui/core';
import { RouteComponentProps } from '@reach/router';
import { useUsernameContext } from '../providers/UsernameProvider';

const Container = styled.div`
    min-height: 75vh;
    display: grid;
    place-items: center;
`;

const FormCard = styled(Card)`
    && {
        padding: 1rem;
        display: flex;
        flex-direction: column;
        gap: 0.5rem;
    }
`;

const Welcome: React.FC<RouteComponentProps> = () => {
    const [tempUsername, setTempUsername] = useState<string>();
    const [error, setError] = useState<string | null>(null);
    const { setUsername } = useUsernameContext();

    const onSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (tempUsername && tempUsername.length < 20) {
            setUsername(tempUsername);
        }
    };

    const handleChangeUsername = (username: string) => {
        setTempUsername(username);
        setError(tempUsername && tempUsername.length >= 20 ? 'Provided username is too long!' : null);
    };

    return (
        <Container>
            <form onSubmit={onSubmit}>
                <FormCard>
                    <TextField
                        error={!!error}
                        helperText={error}
                        label="Username"
                        value={tempUsername || ''}
                        onChange={(event) => handleChangeUsername(event.target.value)}
                        required
                    />
                    <Button type="submit" variant="contained" color="primary">
                        Start
                    </Button>
                </FormCard>
            </form>
        </Container>
    );
};

export default Welcome;
