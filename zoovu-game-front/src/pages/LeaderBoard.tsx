import React from 'react';
import {
    Card,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
} from '@material-ui/core';
import { formatDate } from '../utils/utils';
import styled from 'styled-components';
import TablePaginationActions from '@material-ui/core/TablePagination/TablePaginationActions';
import { DEFAULT_PAGINATION, EMPTY_MESSAGE, ERROR_MESSAGE } from '../common/consts';
import TableLoadingRows from '../components/table/TableLoadingRows';
import TableEmptyRows from '../components/table/TableEmptyRows';
import useLoaderBoard from '../hooks/useLeaderBoard';
import TableState from '../components/table/TableState';
import DataRangePicker from '../components/DateRangePicker';
import TableToolbar from '../components/table/TableToolbar';
import { RouteComponentProps } from '@reach/router';

const THIN_CELL_WIDTH = '2rem';
const THICK_CELL_WIDTH = '10rem';
const MEDIUM_CELL_WIDTH = '7.5rem';

const Container = styled.div`
    min-height: 75vh;
    display: grid;
    place-items: center;
    padding: 5rem;
`;

const TableCellHeaderContent = styled.span`
    font-weight: ${(props) => props.theme.typography.weight.bold};
    color: ${(props) => props.theme.colors.violet};
`;

interface CustomTableCellProps {
    width: string;
}

const CustomTableCell = styled(TableCell)<CustomTableCellProps>`
    && {
        min-width: ${(props) => props.width};
        width: ${(props) => props.width};
        max-width: ${(props) => props.width};
    }
`;

const COL_SPAN = 5;

const LeaderBoard: React.FC<RouteComponentProps> = () => {
    const {
        loading,
        error,
        isEmpty,
        count,
        games,
        page,
        rowsPerPage,
        dateRange,
        emptyRows,
        refetch,
        handleChangePage,
        handleChangeRowsPerPage,
        handleChangeDateRange,
    } = useLoaderBoard();

    return (
        <Container>
            <Card>
                <TableToolbar title="Leader board">
                    <DataRangePicker dataRange={dateRange} onChange={handleChangeDateRange} maxDate={new Date()} />
                </TableToolbar>
                <TableContainer>
                    <Table size="medium">
                        <TableHead>
                            <TableRow>
                                <CustomTableCell align="left" width={THIN_CELL_WIDTH}>
                                    <TableCellHeaderContent>No.</TableCellHeaderContent>
                                </CustomTableCell>
                                <CustomTableCell align="left" width={THICK_CELL_WIDTH}>
                                    <TableCellHeaderContent>Username</TableCellHeaderContent>
                                </CustomTableCell>
                                <CustomTableCell align="right" width={MEDIUM_CELL_WIDTH}>
                                    <TableCellHeaderContent>Game result (s)</TableCellHeaderContent>
                                </CustomTableCell>
                                <CustomTableCell align="right" width={MEDIUM_CELL_WIDTH}>
                                    <TableCellHeaderContent>Penalties number</TableCellHeaderContent>
                                </CustomTableCell>
                                <CustomTableCell align="left" width={THICK_CELL_WIDTH}>
                                    <TableCellHeaderContent>Played at</TableCellHeaderContent>
                                </CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {games.map((game, index) => (
                                <TableRow key={game.id}>
                                    <CustomTableCell align="left" width={THIN_CELL_WIDTH}>
                                        {page * rowsPerPage + index + 1}
                                    </CustomTableCell>
                                    <CustomTableCell align="left" width={THICK_CELL_WIDTH}>
                                        {game.username}
                                    </CustomTableCell>
                                    <CustomTableCell align="right" width={MEDIUM_CELL_WIDTH}>
                                        {game.gameResult}
                                    </CustomTableCell>
                                    <CustomTableCell align="right" width={MEDIUM_CELL_WIDTH}>
                                        {game.penaltiesNumber}
                                    </CustomTableCell>
                                    <CustomTableCell align="left" width={THICK_CELL_WIDTH}>
                                        {formatDate(new Date(game.playedAt))}
                                    </CustomTableCell>
                                </TableRow>
                            ))}
                            <TableState
                                error={error}
                                isEmpty={isEmpty && !loading}
                                errorMessage={ERROR_MESSAGE}
                                emptyMessage={EMPTY_MESSAGE}
                                colSpan={COL_SPAN}
                                rowsPerPage={rowsPerPage}
                                refetchData={refetch}
                            />
                            <TableEmptyRows
                                show={!loading && !error && !isEmpty}
                                emptyRowsNumber={emptyRows}
                                colSpan={COL_SPAN}
                            />
                            <TableLoadingRows show={loading} rowsPerPage={emptyRows} colSpan={COL_SPAN} />
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    colSpan={COL_SPAN}
                                    rowsPerPageOptions={DEFAULT_PAGINATION}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    count={count}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                    ActionsComponent={TablePaginationActions}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
            </Card>
        </Container>
    );
};

export default LeaderBoard;
