import React from 'react';
import styled from 'styled-components';
import SquareBorderedCard from '../components/cards/SquareBorderedCard';
import DroppableLetter from '../components/drag_and_drop/DroppableLetter';
import { DragDropContext } from 'react-beautiful-dnd';
import DraggableLetter from '../components/drag_and_drop/DraggableLetter';
import useBoard from '../hooks/useBoard/useBoard';
import TopBar from '../components/TopBar';
import SuccessModal from '../components/SuccessModal';
import { RouteComponentProps } from '@reach/router';
import withUsername from '../hocs/withUserName';

const CardsContainer = styled.div`
    display: flex;
    flex-flow: row wrap;
    justify-content: center;
    gap: 1rem;
    margin: 5rem 0;
`;

const BoardContainer = styled.div`
    display: flex;
    flex-flow: column wrap;
    justify-content: space-between;
    align-items: center;
`;

const Board: React.FC<RouteComponentProps> = () => {
    const {
        username,
        isGameFinished,
        isGameSaved,
        penaltiesNumber,
        timerSeconds,
        sourceDroppablesOrder,
        destinationDroppablesOrder,
        onDragStart,
        onDragEnd,
        isDroppableDisabled,
        getDraggables,
        restartGame,
        saveGame,
    } = useBoard();

    return (
        <>
            <SuccessModal
                timerSeconds={timerSeconds}
                penaltiesNumber={penaltiesNumber}
                username={username}
                isGameFinished={isGameFinished}
                isGameSaved={isGameSaved}
                restartGame={restartGame}
                saveGame={saveGame}
            />
            <TopBar username={username} seconds={timerSeconds} penalties={penaltiesNumber} />
            <DragDropContext onDragEnd={onDragEnd} onDragStart={onDragStart}>
                <BoardContainer>
                    <CardsContainer>
                        {sourceDroppablesOrder.map((droppableId) => (
                            <DroppableLetter
                                id={droppableId}
                                key={droppableId}
                                isDropDisabled={isDroppableDisabled('source', droppableId)}
                            >
                                <SquareBorderedCard type="primary">
                                    {getDraggables(
                                        'source',
                                        droppableId,
                                        (key, index, draggable) =>
                                            draggable && (
                                                <DraggableLetter
                                                    key={key}
                                                    id={draggable.id}
                                                    index={index}
                                                    url={draggable.url}
                                                    isDragDisabled={draggable.isDragDisabled}
                                                />
                                            ),
                                    )}
                                </SquareBorderedCard>
                            </DroppableLetter>
                        ))}
                    </CardsContainer>
                    <CardsContainer>
                        {destinationDroppablesOrder.map((droppableId) => (
                            <DroppableLetter
                                id={droppableId}
                                key={droppableId}
                                isDropDisabled={isDroppableDisabled('destination', droppableId)}
                            >
                                <SquareBorderedCard type="secondary">
                                    {getDraggables(
                                        'destination',
                                        droppableId,
                                        (key, index, draggable) =>
                                            draggable && (
                                                <DraggableLetter
                                                    key={key}
                                                    id={draggable.id}
                                                    index={index}
                                                    url={draggable.url}
                                                    isDragDisabled={draggable.isDragDisabled}
                                                />
                                            ),
                                    )}
                                </SquareBorderedCard>
                            </DroppableLetter>
                        ))}
                    </CardsContainer>
                </BoardContainer>
            </DragDropContext>
        </>
    );
};

export default withUsername(Board);
