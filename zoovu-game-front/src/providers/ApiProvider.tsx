import React from 'react';
import axios, { AxiosInstance } from 'axios';
import { useSnackbar } from 'notistack';

const errorComposer = (error: any, enqueueSnackbar: (text: string) => void) => {
    const statusCode = error.response ? error.response.status : null;
    if (statusCode) {
        const statusCodeType = statusCode.toString()[0];
        if (statusCodeType == 5) {
            enqueueSnackbar('Error occurred - caused by server');
        } else if (statusCodeType == 4) {
            enqueueSnackbar('Error occurred - caused by client');
        } else {
            enqueueSnackbar('Error occurred');
        }
    }
};

interface ApiResult {
    api: AxiosInstance;
}

const ApiContext = React.createContext<null | ApiResult>(null);

interface ApiProviderProps {
    children: React.ReactNode;
}

export const ApiProvider = ({ children }: ApiProviderProps): React.ReactElement => {
    const { enqueueSnackbar } = useSnackbar();

    const api = axios.create({
        baseURL: 'http://localhost:8080',
    });

    api.interceptors.response.use(
        (response) => response,
        (error) => {
            errorComposer(error, (text: string) => enqueueSnackbar(text, { variant: 'error' }));
        },
    );

    const value = { api } as ApiResult;

    return <ApiContext.Provider value={value}>{children}</ApiContext.Provider>;
};

export const useApiContext = (): ApiResult => {
    const context = React.useContext(ApiContext);
    if (!context) {
        throw new Error('useApiContext must be used within a ApiProvider');
    }
    return context;
};
