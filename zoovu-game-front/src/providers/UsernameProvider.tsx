import React, { useState } from 'react';

interface UsernameResult {
    username: string;
    setUsername: (username: string) => void;
}

const UsernameContext = React.createContext<null | UsernameResult>(null);

interface UsernameProviderProps {
    children: React.ReactNode;
}

export const UsernameProvider = ({ children }: UsernameProviderProps): React.ReactElement => {
    const [username, setUsername] = useState<string>();

    const value = { username, setUsername } as UsernameResult;

    return <UsernameContext.Provider value={value}>{children}</UsernameContext.Provider>;
};

export const useUsernameContext = (): UsernameResult => {
    const context = React.useContext(UsernameContext);
    if (!context) {
        throw new Error('useUsernameContext must be used within a UsernameProvider');
    }
    return context;
};
