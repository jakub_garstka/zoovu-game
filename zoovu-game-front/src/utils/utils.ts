export const shuffleArray = <T>(array: T[]): T[] => {
    const newArray = [...array];
    for (let i = newArray.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
    }
    return newArray;
};

const createZerosPrefix = (zerosNum = 1): string => (zerosNum > 1 ? `${createZerosPrefix(zerosNum - 1)}0` : '0');

const addLeadingZeros = (number: number, minLength = 2): string => {
    const numberLength = number.toString().length;
    if (numberLength < minLength) {
        const zerosPrefix = createZerosPrefix(minLength - numberLength);
        return `${zerosPrefix}${number}`;
    }

    return `${number}`;
};

export const formatDate = (date: Date, withTime = true): string => {
    const day = addLeadingZeros(date.getDate());
    const month = addLeadingZeros(date.getMonth() + 1);
    const year = addLeadingZeros(date.getFullYear(), 4);

    const hours = addLeadingZeros(date.getHours());
    const minutes = addLeadingZeros(date.getMinutes());

    if (withTime) return `${day}/${month}/${year} ${hours}:${minutes}`;

    return `${day}/${month}/${year}`;
};
