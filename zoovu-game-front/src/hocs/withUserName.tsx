import React from 'react';
import { useUsernameContext } from '../providers/UsernameProvider';
import Welcome from '../pages/Welcome';

const withUsername = <P extends Record<string, unknown>>(
    Component: React.ComponentType<P>,
    // eslint-disable-next-line react/display-name
): React.FC<P> => (props: P) => {
    const { username } = useUsernameContext();
    return !username ? <Welcome /> : <Component {...(props as P)} />;
};

export default withUsername;
