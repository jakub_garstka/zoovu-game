import { formatDate } from '../utils/utils';

const day = 1;
const month = 1;
const year = 2021;

const hour = 1;
const minute = 1;

test('date should include some time', () => {
    //give
    const date = new Date(year, month, day, hour, minute);

    //when
    const formattedDate = formatDate(date);

    //then
    expect(formattedDate).toEqual(`0${day}/0${month + 1}/${year} 0${hour}:0${minute}`);
});

test('date should not include any time', () => {
    //give
    const date = new Date(year, month, day, hour, minute);

    //when
    const formattedDate = formatDate(date, false);

    //then
    expect(formattedDate).toEqual(`0${day}/0${month + 1}/${year}`);
});
