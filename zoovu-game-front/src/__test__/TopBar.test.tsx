import React from 'react';
import renderWithTheme from './renderWithTheme';
import TopBar from '../components/TopBar';

test('top bar should display given seconds and username', () => {
    //given
    const seconds = 1;
    const penalties = 0;
    const username = 'username';

    //when
    const { getByText } = renderWithTheme(<TopBar username={username} seconds={seconds} penalties={penalties} />);
    const secondsText = getByText(`Your score: ${seconds} seconds`);
    const usernameText = getByText(`Good luck, ${username}!`);

    //then
    expect(secondsText).toBeInTheDocument();
    expect(usernameText).toBeInTheDocument();
});
