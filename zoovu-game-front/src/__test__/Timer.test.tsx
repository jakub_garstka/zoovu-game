import React from 'react';
import Timer from '../components/Timer';
import renderWithTheme from './renderWithTheme';

test('timer should display given seconds', () => {
    //given
    const seconds = 1;
    const penalties = 0;

    //when
    const { getByText } = renderWithTheme(<Timer seconds={seconds} penalties={penalties} />);
    const secondsText = getByText(`Your score: ${seconds} seconds`);

    //then
    expect(secondsText).toBeInTheDocument();
});
