import { ThemeProvider } from 'styled-components';
import { theme as defaultTheme } from '../styles/theme';
import { render, RenderOptions, RenderResult } from '@testing-library/react';
import React from 'react';

const TestThemeProvider = ({ children }: any) => {
    return <ThemeProvider theme={defaultTheme}>{children}</ThemeProvider>;
};

const renderWithTheme = (ui: React.ReactElement, options?: Omit<RenderOptions, 'queries'>): RenderResult =>
    render(ui, { wrapper: TestThemeProvider, ...options });

export default renderWithTheme;
