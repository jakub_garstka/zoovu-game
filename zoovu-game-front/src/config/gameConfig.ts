import ZoovuO from '../assets/zoovu-o.svg';
import ZoovuU from '../assets/zoovu-u.svg';
import ZoovuV from '../assets/zoovu-v.svg';
import ZoovuZ from '../assets/zoovu-z.svg';

const LETTERS_IDS = {
    letterO: 'letter_o',
    letterO_: 'letter_o_',
    letterZ: 'letter_z',
    letterU: 'letter_u',
    letterV: 'letter_v',
};

const SOURCE_COLUMNS_IDS = {
    sourceO: 'source_o',
    sourceO_: 'source_o_',
    sourceZ: 'source_z',
    sourceU: 'source_u',
    sourceV: 'source_v',
};

const DESTINATION_COLUMNS_IDS = {
    destinationZ: 'destination_z',
    destinationO: 'destination_o',
    destinationO_: 'destination_o_',
    destinationV: 'destination_v',
    destinationU: 'destination_u',
};

export interface Droppable {
    id: string;
    lettersIds: string[];
}

export interface DestinationDroppable extends Droppable {
    allowedLettersIds: string[];
}

export interface Draggable {
    id: string;
    url: string;
}

export interface Droppables {
    source: Record<string, Droppable>;
    destination: Record<string, DestinationDroppable>;
}

export interface GameConfig {
    draggables: Record<string, Draggable>;
    droppables: Droppables;
    orders: {
        source?: string[];
        destination: string[];
    };
}

export const GAME_CONFIG: GameConfig = {
    draggables: {
        [LETTERS_IDS.letterZ]: {
            id: LETTERS_IDS.letterZ,
            url: ZoovuZ,
        },
        [LETTERS_IDS.letterO]: {
            id: LETTERS_IDS.letterO,
            url: ZoovuO,
        },
        [LETTERS_IDS.letterO_]: {
            id: LETTERS_IDS.letterO_,
            url: ZoovuO,
        },
        [LETTERS_IDS.letterV]: {
            id: LETTERS_IDS.letterV,
            url: ZoovuV,
        },
        [LETTERS_IDS.letterU]: {
            id: LETTERS_IDS.letterU,
            url: ZoovuU,
        },
    },
    droppables: {
        source: {
            [SOURCE_COLUMNS_IDS.sourceO]: {
                id: SOURCE_COLUMNS_IDS.sourceO,
                lettersIds: [LETTERS_IDS.letterO],
            },
            [SOURCE_COLUMNS_IDS.sourceO_]: {
                id: SOURCE_COLUMNS_IDS.sourceO_,
                lettersIds: [LETTERS_IDS.letterO_],
            },
            [SOURCE_COLUMNS_IDS.sourceZ]: {
                id: SOURCE_COLUMNS_IDS.sourceZ,
                lettersIds: [LETTERS_IDS.letterZ],
            },
            [SOURCE_COLUMNS_IDS.sourceU]: {
                id: SOURCE_COLUMNS_IDS.sourceU,
                lettersIds: [LETTERS_IDS.letterU],
            },
            [SOURCE_COLUMNS_IDS.sourceV]: {
                id: SOURCE_COLUMNS_IDS.sourceV,
                lettersIds: [LETTERS_IDS.letterV],
            },
        },
        destination: {
            [DESTINATION_COLUMNS_IDS.destinationZ]: {
                id: DESTINATION_COLUMNS_IDS.destinationZ,
                allowedLettersIds: [LETTERS_IDS.letterZ],
                lettersIds: [],
            },
            [DESTINATION_COLUMNS_IDS.destinationO]: {
                id: DESTINATION_COLUMNS_IDS.destinationO,
                allowedLettersIds: [LETTERS_IDS.letterO, LETTERS_IDS.letterO_],
                lettersIds: [],
            },
            [DESTINATION_COLUMNS_IDS.destinationO_]: {
                id: DESTINATION_COLUMNS_IDS.destinationO_,
                allowedLettersIds: [LETTERS_IDS.letterO, LETTERS_IDS.letterO_],
                lettersIds: [],
            },
            [DESTINATION_COLUMNS_IDS.destinationV]: {
                id: DESTINATION_COLUMNS_IDS.destinationV,
                allowedLettersIds: [LETTERS_IDS.letterV],
                lettersIds: [],
            },
            [DESTINATION_COLUMNS_IDS.destinationU]: {
                id: DESTINATION_COLUMNS_IDS.destinationU,
                allowedLettersIds: [LETTERS_IDS.letterU],
                lettersIds: [],
            },
        },
    },
    orders: {
        destination: Object.values(DESTINATION_COLUMNS_IDS),
    },
};
