export const DEFAULT_PAGINATION = [5, 10, 15, 20];

export const ERROR_MESSAGE = 'Some error occurred, try again!';
export const EMPTY_MESSAGE = 'There is no data in the given criteria, try again!';

export const REFETCH_DATA = 'Refetch Data';

export const DATE = 'Date';
export const ALL = 'All';
