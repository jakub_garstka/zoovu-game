export interface Game {
    id?: string;
    username: string;
    gameResult: number;
    penaltiesNumber: number;
    playedAt: Date;
}

export interface PaginationResult<T> {
    list: T[];
    count: number;
}
