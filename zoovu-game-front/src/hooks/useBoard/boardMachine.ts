import { DestinationDroppable, Draggable, Droppable, GameConfig } from '../../config/gameConfig';
import { DragStart, DropResult } from 'react-beautiful-dnd';
import { shuffleArray } from '../../utils/utils';

export type ContextGameConfigPathType = 'source' | 'destination';

interface DisablingContextDroppable {
    isDropDisabled: boolean;
}

interface DisablingContextDraggable {
    isDragDisabled: boolean;
}

export interface ContextDroppable extends Droppable, DisablingContextDroppable {}

export interface ContextDestinationDroppable extends DestinationDroppable, DisablingContextDroppable {}

export interface ContextDraggable extends Draggable, DisablingContextDraggable {}

export interface ContextDroppables {
    source: Record<string, ContextDroppable>;
    destination: Record<string, ContextDestinationDroppable>;
}

export interface ContextMove {
    sourceId: string;
    destinationId: string;
    draggableId: string;
}

export interface ContextGameConfig {
    penaltiesNumber: number;
    isGameFinished: boolean;
    finishDate?: Date;
    moves: ContextMove[];
    draggables: Record<string, ContextDraggable>;
    droppables: ContextDroppables;
    orders: {
        source: string[];
        destination: string[];
    };
}

export interface BoardStateSchema {
    states: {
        idle: Record<string, unknown>;
        dragStarted: Record<string, unknown>;
        dragEnded: Record<string, unknown>;
        finished: Record<string, unknown>;
    };
}

export enum GameEventTypeEnum {
    DRAG_START = 'DRAG_START',
    DRAG_END = 'DRAG_END',
    RESTART = 'RESTART',
}

export type GameEventDragStartType = { type: GameEventTypeEnum.DRAG_START; payload: DragStart };
export type GameEventDragEndType = { type: GameEventTypeEnum.DRAG_END; payload: DropResult };
export type GameEventRestartType = { type: GameEventTypeEnum.RESTART };

export type GameEvent = GameEventDragStartType | GameEventDragEndType | GameEventRestartType;

export enum DroppableIdPrefix {
    SOURCE = 'SOURCE/',
    DESTINATION = 'DESTINATION/',
}

const convertDroppableToContextDroppable = <K extends Droppable, T extends ContextDroppable & K>(
    droppables: Record<string, K>,
    droppableIdPrefix: DroppableIdPrefix,
): Record<string, T> => {
    return Object.entries(droppables).reduce((acc, [key, value]) => {
        return {
            ...acc,
            [`${droppableIdPrefix}${key}`]: {
                ...value,
                isDropDisabled: false,
            } as T,
        };
    }, {});
};

const convertDraggableToContextDraggable = <K extends Draggable, T extends ContextDraggable & K>(
    draggables: Record<string, K>,
): Record<string, T> => {
    return Object.entries(draggables).reduce((acc, [key, value]) => {
        return {
            ...acc,
            [key]: {
                ...value,
                isDragDisabled: false,
            } as T,
        };
    }, {});
};

export const prepareContextGameConfig = (gameConfig: GameConfig): ContextGameConfig => {
    const { orders, draggables, droppables } = gameConfig;
    const { source: sourceDroppables, destination: destinationDroppables } = droppables;

    return {
        penaltiesNumber: 0,
        isGameFinished: false,
        moves: [],
        orders: {
            source: (orders.source || shuffleArray(Object.keys(sourceDroppables))).map(
                (id) => `${DroppableIdPrefix.SOURCE}${id}`,
            ),
            destination: orders.destination.map((id) => `${DroppableIdPrefix.DESTINATION}${id}`),
        },
        draggables: convertDraggableToContextDraggable(draggables),
        droppables: {
            source: convertDroppableToContextDroppable(sourceDroppables, DroppableIdPrefix.SOURCE),
            destination: convertDroppableToContextDroppable(destinationDroppables, DroppableIdPrefix.DESTINATION),
        },
    };
};
