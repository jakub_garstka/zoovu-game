import React, { useState } from 'react';
import cloneDeep from 'lodash/cloneDeep';
import { GAME_CONFIG } from '../../config/gameConfig';
import { DragStart, DropResult } from 'react-beautiful-dnd';
import { useMachine } from '@xstate/react';
import { actions, Machine } from 'xstate';
import {
    ContextGameConfig,
    BoardStateSchema,
    GameEvent,
    GameEventDragEndType,
    GameEventDragStartType,
    GameEventTypeEnum,
    ContextDestinationDroppable,
    ContextDroppables,
    ContextDroppable,
    ContextDraggable,
    ContextMove,
    DroppableIdPrefix,
    ContextGameConfigPathType,
    prepareContextGameConfig,
} from './boardMachine';
import useTimer from '../useTimer';
import { useApiContext } from '../../providers/ApiProvider';
import { useUsernameContext } from '../../providers/UsernameProvider';
import { Game } from '../../common/types';
import { GAMES_BASE_URL } from '../../common/endpoints';
import { useSnackbar } from 'notistack';

const { assign } = actions;

interface UseBoardResult {
    username: string;
    timerSeconds: number;
    penaltiesNumber: number;
    isGameFinished: boolean;
    isGameSaved: boolean;
    onDragStart: (initial: DragStart) => void;
    onDragEnd: (result: DropResult) => void;
    sourceDroppablesOrder: string[];
    destinationDroppablesOrder: string[];
    isDroppableDisabled: (path: ContextGameConfigPathType, droppableId: string) => boolean;
    getDraggables: (
        path: ContextGameConfigPathType,
        droppableId: string,
        createLetterFunction: (key: string, index: number, draggable: ContextDraggable | null) => React.ReactNode,
    ) => React.ReactNode;
    restartGame: () => void;
    saveGame: () => void;
}

const isDraggedFromSourceCondition = (context: ContextGameConfig, event: GameEventDragStartType): boolean => {
    const { source } = event.payload;
    return source.droppableId.indexOf(DroppableIdPrefix.SOURCE) !== -1;
};

const isMoveCorrectCondition = (context: ContextGameConfig, event: GameEventDragEndType): boolean => {
    const { destination, draggableId } = event.payload;
    if (destination) {
        const droppables = context.droppables;
        const { allowedLettersIds } = getDroppable(droppables, destination.droppableId) as ContextDestinationDroppable;

        return allowedLettersIds.some((allowedLetterId) => allowedLetterId === draggableId);
    }
    return true;
};

const isMoveWinningCondition = (context: ContextGameConfig): boolean => {
    return !Object.values(context.droppables.destination).some(
        (droppable) =>
            droppable.lettersIds.length !== 1 || droppable.allowedLettersIds.indexOf(droppable.lettersIds[0]) === -1,
    );
};

const getDroppablePath = (droppableId: string): ContextGameConfigPathType => {
    return droppableId.indexOf(DroppableIdPrefix.SOURCE) !== -1 ? 'source' : 'destination';
};

const getDroppable = (droppables: ContextDroppables, droppableId: string): ContextDroppable => {
    const path: ContextGameConfigPathType = getDroppablePath(droppableId);
    return {
        ...droppables[path][droppableId],
    };
};

const replaceDraggableBetweenSourceDroppables = (
    droppables: ContextDroppables,
    sourceDroppableId: string,
    sourceDroppable: ContextDroppable,
    destinationDroppableId: string,
    destinationDroppable: ContextDestinationDroppable,
) => {
    return {
        ...droppables,
        source: {
            ...droppables.source,
            [sourceDroppableId]: {
                ...sourceDroppable,
                lettersIds: [...destinationDroppable.lettersIds],
            },
        },
        destination: {
            ...droppables.destination,
            [destinationDroppableId]: {
                ...(destinationDroppable as ContextDestinationDroppable),
                lettersIds: [...sourceDroppable.lettersIds],
            },
        },
    };
};

const replaceDraggableBetweenDestinationDroppables = (
    droppables: ContextDroppables,
    sourceDroppableId: string,
    sourceDroppable: ContextDestinationDroppable,
    destinationDroppableId: string,
    destinationDroppable: ContextDestinationDroppable,
) => {
    return {
        ...droppables,
        destination: {
            ...droppables.destination,
            [sourceDroppableId]: {
                ...sourceDroppable,
                lettersIds: [...destinationDroppable.lettersIds],
            },
            [destinationDroppableId]: {
                ...destinationDroppable,
                lettersIds: [...sourceDroppable.lettersIds],
            },
        },
    };
};

const moveAction = (context: ContextGameConfig, event: GameEventDragEndType): ContextGameConfig => {
    const { destination, draggableId, source } = event.payload;
    if (destination) {
        const { droppables } = context;

        const sourcePath = getDroppablePath(source.droppableId);

        const sourceDroppable = getDroppable(droppables, source.droppableId);
        const destinationDroppable = getDroppable(droppables, destination.droppableId);

        const tempDroppables: ContextDroppables =
            sourcePath === 'destination'
                ? replaceDraggableBetweenDestinationDroppables(
                      droppables,
                      source.droppableId,
                      sourceDroppable as ContextDestinationDroppable,
                      destination.droppableId,
                      destinationDroppable as ContextDestinationDroppable,
                  )
                : replaceDraggableBetweenSourceDroppables(
                      droppables,
                      source.droppableId,
                      sourceDroppable,
                      destination.droppableId,
                      destinationDroppable as ContextDestinationDroppable,
                  );

        const tempMove: ContextMove = {
            sourceId: source.droppableId,
            destinationId: destination.droppableId,
            draggableId: draggableId,
        };

        return {
            ...context,
            moves: [...context.moves, tempMove],
            droppables: tempDroppables,
        };
    }
    return context;
};

const addPenaltyAction = (context: ContextGameConfig, callback?: () => void): ContextGameConfig => {
    callback && callback();
    return {
        ...context,
        penaltiesNumber: context.penaltiesNumber + 1,
    };
};

const finishTheGameAction = (context: ContextGameConfig): ContextGameConfig => {
    return {
        ...context,
        isGameFinished: true,
        finishDate: new Date(),
    };
};

const menageDroppablesAction = (
    context: ContextGameConfig,
    path: ContextGameConfigPathType,
    isDropDisabled: boolean,
    onlyFilled = false,
): ContextGameConfig => {
    const droppables = context.droppables;
    const droppablesInPath = context.droppables[path];

    const tempDroppablesInPath = Object.entries(droppablesInPath).reduce((acc, [key, value]) => {
        return {
            ...acc,
            [key]: {
                ...value,
                isDropDisabled: onlyFilled
                    ? value.lettersIds.length > 0
                        ? isDropDisabled
                        : value.isDropDisabled
                    : isDropDisabled,
            },
        };
    }, {});

    return {
        ...context,
        droppables: {
            ...droppables,
            [path]: tempDroppablesInPath,
        },
    };
};

const menageDraggablesAction = (context: ContextGameConfig, isDragDisabled: boolean): ContextGameConfig => {
    const draggables = { ...context.draggables };
    const tempDraggables = Object.entries(draggables).reduce((acc, [key, value]) => {
        return {
            ...acc,
            [key]: {
                ...value,
                isDragDisabled: isDragDisabled,
            },
        };
    }, {});

    return {
        ...context,
        draggables: {
            ...tempDraggables,
        },
    };
};

const menageAllAction = (context: ContextGameConfig, isDisbaled: boolean): ContextGameConfig => {
    return {
        ...context,
        draggables: { ...menageDraggablesAction(context, isDisbaled).draggables },
        droppables: {
            source: { ...menageDroppablesAction(context, 'source', isDisbaled).droppables.source },
            destination: { ...menageDroppablesAction(context, 'destination', isDisbaled).droppables.destination },
        },
    };
};

const blockFilledTargetDroppablesAction = (context: ContextGameConfig, event: GameEventDragStartType) =>
    isDraggedFromSourceCondition(context, event) ? menageDroppablesAction(context, 'destination', true, true) : context;

const prepareContextAction = (): ContextGameConfig => {
    return prepareContextGameConfig(cloneDeep(GAME_CONFIG));
};

const addPenaltyIfMoveWasIncorrectAction = (
    context: ContextGameConfig,
    event: GameEventDragEndType,
    callback?: () => void,
): ContextGameConfig => (isMoveCorrectCondition(context, event) ? context : addPenaltyAction(context, callback));

const boardStateMachine = Machine<ContextGameConfig, BoardStateSchema, GameEvent>({
    id: 'board',
    initial: 'idle',
    context: prepareContextAction(),
    states: {
        idle: {
            exit: ['startTimerAction'],
            on: {
                DRAG_START: 'dragStarted',
            },
        },
        dragStarted: {
            entry: ['blockSourceDroppablesAction', 'blockFilledTargetDroppablesAction'],
            on: {
                DRAG_END: 'dragEnded',
            },
        },
        dragEnded: {
            entry: ['moveAction', 'addPenaltyIfMoveWasIncorrectAction'],
            exit: ['unblockAllAction'],
            always: [{ target: 'finished', cond: 'isMoveWinningCondition' }, { target: 'idle' }],
        },
        finished: {
            entry: ['stopTimerAction', 'blockAllAction', 'finishTheGameAction'],
            exit: ['unblockAllAction', 'restartTimerAction', 'prepareContextAction'],
            on: {
                RESTART: 'idle',
            },
        },
    },
});

const useBoard = (): UseBoardResult => {
    const [isGameSaved, setIsGameSaved] = useState(false);
    const { timerSeconds, stopTimer, startTimer, restartTimer, addPenaltyTimeToTimer } = useTimer({});
    const { api } = useApiContext();
    const { username } = useUsernameContext();
    const { enqueueSnackbar } = useSnackbar();

    const boardMachineOptions: Partial<unknown> = {
        actions: {
            startTimerAction: startTimer,
            stopTimerAction: stopTimer,
            restartTimerAction: restartTimer,
            prepareContextAction: assign<ContextGameConfig, GameEventDragEndType>(prepareContextAction),
            moveAction: assign<ContextGameConfig, GameEventDragEndType>(moveAction),
            finishTheGameAction: assign<ContextGameConfig, GameEventDragEndType>(finishTheGameAction),
            blockFilledTargetDroppablesAction: assign<ContextGameConfig, GameEventDragStartType>(
                blockFilledTargetDroppablesAction,
            ),
            blockAllAction: assign<ContextGameConfig, GameEventDragEndType>((context) =>
                menageAllAction(context, true),
            ),
            addPenaltyAction: assign<ContextGameConfig, GameEventDragEndType>((context) =>
                addPenaltyAction(context, addPenaltyTimeToTimer),
            ),
            addPenaltyIfMoveWasIncorrectAction: assign<ContextGameConfig, GameEventDragEndType>((context, event) =>
                addPenaltyIfMoveWasIncorrectAction(context, event, addPenaltyTimeToTimer),
            ),
            blockSourceDroppablesAction: assign<ContextGameConfig, GameEventDragEndType>((context) =>
                menageDroppablesAction(context, 'source', true),
            ),
            unblockAllAction: assign<ContextGameConfig, GameEventDragEndType>((context) =>
                menageAllAction(context, false),
            ),
        },
        guards: {
            isMoveWinningCondition,
        },
    };

    const [current, send] = useMachine(boardStateMachine, boardMachineOptions);

    const getDraggables = (
        path: ContextGameConfigPathType,
        droppableId: string,
        createDraggableFunction: (key: string, index: number, draggable: ContextDraggable | null) => React.ReactNode,
    ): React.ReactNode => {
        return current.context.droppables[path][droppableId].lettersIds.map((letterId, index) => {
            const letter = current.context.draggables[letterId] || null;
            return createDraggableFunction(letterId, index, letter);
        });
    };

    const isDroppableDisabled = (path: ContextGameConfigPathType, droppableId: string) => {
        return current.context.droppables[path][droppableId].isDropDisabled || false;
    };

    const onDragStart = (dragStart: DragStart) => {
        send({ type: GameEventTypeEnum.DRAG_START, payload: dragStart });
    };

    const onDragEnd = (dropResult: DropResult) => {
        send({ type: GameEventTypeEnum.DRAG_END, payload: dropResult });
    };

    const restartGame = () => {
        send({ type: GameEventTypeEnum.RESTART });
        setIsGameSaved(false);
    };

    const saveGame = async () => {
        try {
            const game = {
                username,
                gameResult: timerSeconds,
                penaltiesNumber: current.context.penaltiesNumber,
                playedAt: current.context.finishDate,
            } as Game;
            await api.post<Game>(GAMES_BASE_URL, game);
            setIsGameSaved(true);
            enqueueSnackbar('Your game is saved successfully!');
        } catch (_) {}
    };

    return {
        username,
        timerSeconds,
        penaltiesNumber: current.context.penaltiesNumber,
        isGameFinished: current.context.isGameFinished,
        isGameSaved,
        sourceDroppablesOrder: current.context.orders.source,
        destinationDroppablesOrder: current.context.orders.destination,
        onDragStart,
        onDragEnd,
        isDroppableDisabled,
        getDraggables,
        restartGame,
        saveGame,
    } as UseBoardResult;
};

export default useBoard;
