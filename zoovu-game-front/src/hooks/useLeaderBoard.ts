import { useApiContext } from '../providers/ApiProvider';
import React, { useEffect, useState } from 'react';
import { Game, PaginationResult } from '../common/types';
import { DEFAULT_PAGINATION } from '../common/consts';
import { GAMES_BASE_URL } from '../common/endpoints';
import { DateRange } from 'materialui-daterange-picker/dist';

interface UseLoaderBoardResult {
    loading: boolean;
    error: boolean;
    isEmpty: boolean;
    count: number;
    games: Game[];
    page: number;
    rowsPerPage: number;
    dateRange?: DateRange;
    emptyRows: number;
    refetch: () => void;
    handleChangePage: (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => void;
    handleChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
    handleChangeDateRange: (dataRange?: DateRange) => void;
}

const useLoaderBoard = (): UseLoaderBoardResult => {
    const { api } = useApiContext();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(DEFAULT_PAGINATION[0]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [games, setGames] = useState<Game[]>([]);
    const [count, setCount] = useState(0);
    const [dateRange, setDateRange] = React.useState<DateRange>();

    const countEmptyRows = () => {
        const emptyRows = rowsPerPage - (games?.length || 0);
        return emptyRows > 0 ? emptyRows : 0;
    };

    const fetch = async () =>
        api.get<PaginationResult<Game>>(GAMES_BASE_URL, {
            params: {
                skip: page * rowsPerPage,
                limit: rowsPerPage,
                from: dateRange?.startDate,
                to: dateRange?.endDate,
            },
        });

    const fetchGames = async () => {
        setLoading(true);
        setError(false);
        try {
            const result = await fetch();

            const { list, count } = result.data;
            setGames(list);
            setCount(count);
        } catch (e) {
            setError(true);
            setGames([]);
        }
        setLoading(false);
    };

    useEffect(() => {
        fetchGames();
    }, [page, rowsPerPage, dateRange?.endDate, dateRange?.startDate]);

    const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeDateRange = (dateRange?: DateRange) => {
        setDateRange(dateRange);
    };

    return {
        loading,
        error,
        isEmpty: !(games && games.length > 0),
        count,
        games,
        page,
        rowsPerPage,
        dateRange,
        emptyRows: countEmptyRows(),
        refetch: fetchGames,
        handleChangeDateRange,
        handleChangePage,
        handleChangeRowsPerPage,
    };
};

export default useLoaderBoard;
