import { useLayoutEffect } from 'react';

const useEffectsCleanUp = (effectsCount: number, duration: number, cleanUpFunction: () => void): void => {
    useLayoutEffect(() => {
        let bounce: NodeJS.Timeout | null = null;
        if (effectsCount > 0) {
            bounce && clearTimeout(bounce);
            bounce = setTimeout(() => {
                cleanUpFunction();
                bounce && clearTimeout(bounce);
            }, duration);
        }
        return () => {
            bounce && clearTimeout(bounce);
        };
    }, [effectsCount, duration, cleanUpFunction]);
};

export default useEffectsCleanUp;
