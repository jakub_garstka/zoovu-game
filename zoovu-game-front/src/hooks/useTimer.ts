import { useEffect, useState } from 'react';

interface UseTimerResult {
    timerSeconds: number;
    isTimerStarted: boolean;
    stopTimer: () => void;
    startTimer: () => void;
    restartTimer: () => void;
    addPenaltyTimeToTimer: () => void;
}

interface UseTimerProps {
    penaltyTime?: number;
}

const useTimer = ({ penaltyTime = 10 }: UseTimerProps): UseTimerResult => {
    const [isTimerStarted, setIsTimerStarted] = useState(false);
    const [timerSeconds, setTimerSeconds] = useState(0);
    const [timer, setTimer] = useState<NodeJS.Timeout | null>(null);

    useEffect(() => {
        if (isTimerStarted) {
            setTimer(
                setInterval(() => {
                    setTimerSeconds((seconds) => seconds + 1);
                }, 1000),
            );
        } else if (timer && !isTimerStarted) {
            clearInterval(timer);
        }
        return () => {
            timer && clearInterval(timer);
        };
    }, [isTimerStarted]);

    const startTimer = () => {
        !isTimerStarted && setIsTimerStarted(true);
    };

    const stopTimer = () => {
        isTimerStarted && setIsTimerStarted(false);
    };

    const restartTimer = () => {
        setTimerSeconds(0);
    };

    const addPenaltyTimeToTimer = () => {
        setTimerSeconds((seconds) => seconds + penaltyTime);
    };

    return {
        timerSeconds,
        isTimerStarted,
        stopTimer,
        startTimer,
        restartTimer,
        addPenaltyTimeToTimer,
    } as UseTimerResult;
};

export default useTimer;
