import { createGlobalStyle } from 'styled-components';
import BackgroundImage from '../assets/background-image.png';

const GlobalStyle = createGlobalStyle` 

    html {
        font-size: ${(props) => props.theme.typography.size.normal};
    }
    
    body {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        margin: 0;
        background-image: url(${BackgroundImage});
        background-size: cover;
        background-repeat: no-repeat;
        background-attachment: fixed;
        min-height: 100vh;
        display: flex;
        flex-direction: column;
    } 
    
    * {
        font-family: 'Roboto', sans-serif;
    }
  
`;

export default GlobalStyle;
