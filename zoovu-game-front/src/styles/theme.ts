import { DefaultTheme } from 'styled-components';
import { createMuiTheme } from '@material-ui/core';

const borderRadius = 20;

const theme: DefaultTheme = {
    borderRadius: `${borderRadius}px`,
    boxShadow: '10px 10px 20px #F5F6F8',
    colors: {
        white: '#fdfdfd',
        black: '#323232',
        gray: '#A1ABBD',
        violet: '#3b0078',
        green: '#18E5B1',
    },
    typography: {
        size: {
            small: '0.75rem',
            normal: '1rem',
            large: '1.75rem',
        },
        weight: {
            light: '300',
            bold: '700',
        },
    },
};

const muiTheme = createMuiTheme({
    palette: {
        primary: {
            main: theme.colors.green,
            contrastText: theme.colors.white,
        },
        secondary: {
            main: theme.colors.black,
            contrastText: theme.colors.white,
        },
    },
    shape: {
        borderRadius: borderRadius,
    },
    unstable_strictMode: true,
});

export { theme, muiTheme };
